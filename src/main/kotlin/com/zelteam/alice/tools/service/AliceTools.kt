package com.zelteam.alice.tools.service

import com.zelteam.alice.tools.data.AliceInput
import com.zelteam.alice.tools.data.AliceOutput
import com.zelteam.alice.tools.data.Response
import com.zelteam.alice.tools.data.SessionOutput
import kotlin.text.RegexOption.IGNORE_CASE


fun containsWord(phrase: String, wordsRegexp: String) =
        phrase.toLowerCase().contains(wordsRegexp.toRegex(IGNORE_CASE))


fun buildAliceOutput(text: String, aliceInput: AliceInput, endSession: Boolean = false): AliceOutput {
    return AliceOutput(
            response = Response(
                    text = selectOptions(text),
                    endSession = endSession
            ),
            session = SessionOutput(
                    messageId = aliceInput.session.messageId,
                    sessionId = aliceInput.session.sessionId,
                    userId = aliceInput.session.userId
            ),
            version = aliceInput.version)
}

fun selectOptions(text: String) =
        text.replace("\\{.*?}".toRegex(), transform = { matchResult ->
            matchResult.value.drop(1).dropLast(1).split('/').shuffled()[0]
        })


