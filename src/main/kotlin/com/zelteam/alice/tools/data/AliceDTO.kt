package com.zelteam.alice.tools.data

import com.fasterxml.jackson.annotation.JsonProperty


data class AliceInput(val meta: Meta?,
                      val request: Request,
                      val session: SessionInput,
                      val version: String)

data class AliceOutput(val response: Response,
                       val session: SessionOutput,
                       val version: String)

data class Meta(val locale: String,
                val timezone: String,
                @JsonProperty("client_id") val clientId: String)

data class Request(val command: String,
                   val type: String,
                   @JsonProperty("original_utterance") val originalUtterance: String)

data class Response(val text: String,
                    @JsonProperty("end_session") val endSession: Boolean)

data class SessionInput(val new: Boolean,
                        @JsonProperty("message_id") val messageId: Int,
                        @JsonProperty("session_id") val sessionId: String,
                        @JsonProperty("skill_id") val skillId: String,
                        @JsonProperty("user_id") val userId: String)

data class SessionOutput(@JsonProperty("message_id") val messageId: Int,
                         @JsonProperty("session_id") val sessionId: String,
                         @JsonProperty("user_id") val userId: String)